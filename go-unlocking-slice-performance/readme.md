Unlocking Go Slice Performance: Navigating sync.Pool for Enhanced Efficiency
---

Original:
[https://blog.mike.norgate.xyz/unlocking-go-slice-performance-navigating-sync-pool-for-enhanced-efficiency-7cb63b0b453e](https://blog.mike.norgate.xyz/unlocking-go-slice-performance-navigating-sync-pool-for-enhanced-efficiency-7cb63b0b453e)

When dealing with large slices, a common suggestion is to utilize sync.Pool in order to enhance both performance and memory usage.
However, it's worth noting that the anticipated improvements might not always materialize. 
A quick online search for using a `sync.Pool` with slices yields recommendations similar to the following:

```go
pool := sync.Pool{
    New: func() interface{} {
        return make([]int, 0, 0)
    },
}

s := pool.Get().([]int)
s = s[0:0] //slice back to empty
s = append(s, 123)
pool.Put(s)
```

But let’s delve into benchmarking this approach to see its actual effectiveness:

```go
func BenchmarkSlicePool(b *testing.B) {
	pool := sync.Pool{
		New: func() interface{} {
			return make([]int, 0, 0)
		},
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		s := pool.Get().([]int)
		s = s[0:0] //slice back to empty
		s = append(s, 123)
		pool.Put(s)
	}
}

```

The benchmark results are as follows:

| Benchmark          | ns/op        | alloc bytes / op | alloc times / op |
| ------------------ | ------------ |----------------- | ---------------- |
| BenchmarkSlicePool | 51.79 ns/op  | 24 B/op          | 1 allocs/op      |

Surprisingly, there’s still one allocation per operation. This outcome is contrary to our expectations.
A closer look at a profile offers additional insights.

![test.BenchmarkSlicePool](./image/test.BenchmarkSlicePool.png)

profile of `test.BenchmarkSlicePool`

The profile of `test.BenchmarkSlicePool` reveals calls to `runtime.convTslice` and `runtime.mallocgc`.
Why is this happening? When returning an object to the pool, it requires allocation on the heap.
In the context of slices, this implies allocating the slice header on the heap. 
The slice header occupies 24 bytes:

```go
type slice struct {
    array unsafe.Pointer
    len   int
    cap   int
}
```

### Pitfall 1 — Pooling the Slice Pointer

To address the issue we just identified, a logical step would be to pool the pointer itself.
After all, if the slice header is already on the heap, there’s no need for it to be transferred there again, right?

```go
func BenchmarkSlicePtrPool_1(b *testing.B) {
	pool := sync.Pool{
		New: func() interface{} {
			s := make([]int, 0, 0)
			return &s
		},
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		s := *pool.Get().(*[]int)
		s = s[0:0] //slice back to empty
		s = append(s, 123)
		pool.Put(&s)
	}
}
```

Results:

| Benchmark               | ns/op        | alloc bytes / op | alloc times / op |
| ----------------------- | ------------ |----------------- | ---------------- |
| BenchmarkSlicePtrPool_1 | 48.26 ns/op  | 24 B/op          | 1 allocs/op      |

Yet, there’s still one allocation per operation of 24 bytes.
Why is this happening again? Profiling reveals that this time, we’re calling `runtime.newobject`.

![test.BenchmarkSlicePool_1](./image/test.BenchmarkSlicePool_1.png)

This issue can be further analyzed using the escape analysis with the `-gcflags="-m"` flag:
```
$ go test .  -gcflags="-m"
...
moved to heap: s
```

Since we are taking the address of `s` when putting it into the pool (`pool.Put(&s)`), `s` gets allocated on the heap.

### Pitfall 2 — Returning the Original Pointer

Pooling the pointer introduced one problem while solving another. Let’s attempt an alternative approach: returning the pointer received from `pool.Get()`.
By doing so, we ensure that s remains on the stack, as we never take its address.

```go
func BenchmarkSlicePtrPool_2(b *testing.B) {
	pool := sync.Pool{
		New: func() interface{} {
			s := make([]int, 0, 0)
			return &s
		},
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		ptr := pool.Get().(*[]int)
		s := *ptr
		s = s[0:0]
		s = append(s, 123)
		pool.Put(ptr)
	}
}
```

Results:

| Benchmark               | ns/op        | alloc bytes / op | alloc times / op |
| ----------------------- | ------------ |----------------- | ---------------- |
| BenchmarkSlicePtrPool_2 | 42.58 ns/op  | 8 B/op           | 1 allocs/op      |

Despite the improvement in the number of allocated bytes, there’s still one allocation occurring. 
Now, the allocated memory size is 8 bytes. A more detailed analysis uncovers that this memory allocation stems from invoking `runtime.growslice`. 
This operation is responsible for the 8-byte allocation (given that an `int` occupies 8 bytes).

![test.BenchmarkSlicePool_2](./image/test.BenchmarkSlicePool_2.png)

This accounts for the 8-byte allocation (an `int` is 8 bytes). The pool is being used after appending, causing the capacity of the new stack-allocated slice header to increase. 
However, `ptr` still points to the original slice header with a capacity of 0. Consequently, every iteration necessitates growing the slice, leading to additional memory allocation.

### The Solution — Copy the Header Before Putting

Armed with this understanding, a small modification can be made to the approach. 
If the stack-allocated slice header is copied onto the heap-allocated slice header, the extra allocation can be avoided.
The memory is already allocated, and the pooled header will have the updated capacity.

```go
func BenchmarkSlicePtrPool_3(b *testing.B) {
	pool := sync.Pool{
		New: func() interface{} {
			s := make([]int, 0, 0)
			return &s
		},
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		ptr := pool.Get().(*[]int)
		s := *ptr
		s = s[0:0]
		s = append(s, 123)
		*ptr = s // Copy the stack header over to the heap
		pool.Put(ptr)
	}
}
```

Results:

| Benchmark               | ns/op        | alloc bytes / op | alloc times / op |
| ----------------------- | ------------ |----------------- | ---------------- |
| BenchmarkSlicePtrPool_3 | 12.37 ns/op  | 0 B/op           | 0 allocs/op      |

Finally, there are no more allocations, resulting in a substantial boost in performance. 
The profile now shows zero calls to `runtime.mallocgc`.

![test.BenchmarkSlicePool_3](./image/test.BenchmarkSlicePool_3.png)

### Summary

Though there are a few nuances to be cautious of when pooling slices, a minor adjustment can yield significant performance benefits. This approach is particularly worthwhile when dealing with numerous short-lived slices.

#### Full Test Results

cpu: AMD Ryzen Threadripper 2950X 16-Core Processor
| Benchmark               | ns/op        | alloc bytes / op | alloc times / op |
| ----------------------- | ------------ |----------------- | ---------------- |
| BenchmarkSlicePool      | 51.79 ns/op  | 24 B/op          | 1 allocs/op      |
| BenchmarkSlicePtrPool_1 | 48.26 ns/op  | 24 B/op          | 1 allocs/op      |
| BenchmarkSlicePtrPool_2 | 42.58 ns/op  | 8 B/op           | 1 allocs/op      |
| BenchmarkSlicePtrPool_3 | 12.37 ns/op  | 0 B/op           | 0 allocs/op      |
