module app

go 1.19

require (
	github.com/prometheus/client_golang v1.14.0
	github.com/segmentio/kafka-go v0.4.38
	gitlab.com/ers.net.ru/go-ragel-parser v0.0.5
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/dlclark/regexp2 v1.7.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/jjeffery/kv v0.8.1 // indirect
	github.com/klauspost/compress v1.15.11 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.1 // indirect
	github.com/pierrec/lz4/v4 v4.1.15 // indirect
	github.com/prometheus/client_model v0.3.0 // indirect
	github.com/prometheus/common v0.37.0 // indirect
	github.com/prometheus/procfs v0.8.0 // indirect
	golang.org/x/crypto v0.0.0-20220926161630-eccd6366d1be // indirect
	golang.org/x/net v0.0.0-20220722155237-a158d28d115b // indirect
	golang.org/x/sys v0.0.0-20220928140112-f11e5e49a4ec // indirect
	golang.org/x/text v0.4.0 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
)
