package go_unlocking_slice_performance

import (
	"sync"
	"testing"
)

func BenchmarkSlicePool(b *testing.B) {
	pool := sync.Pool{
		New: func() interface{} {
			return make([]int, 0, 0)
		},
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		s := pool.Get().([]int)
		s = s[0:0] //slice back to empty
		s = append(s, 123)
		pool.Put(s)
	}
}

func BenchmarkSlicePtrPool_1(b *testing.B) {
	pool := sync.Pool{
		New: func() interface{} {
			s := make([]int, 0, 0)
			return &s
		},
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		s := *pool.Get().(*[]int)
		s = s[0:0] //slice back to empty
		s = append(s, 123)
		pool.Put(&s)
	}
}

func BenchmarkSlicePtrPool_2(b *testing.B) {
	pool := sync.Pool{
		New: func() interface{} {
			s := make([]int, 0, 0)
			return &s
		},
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		ptr := pool.Get().(*[]int)
		s := *ptr
		s = s[0:0]
		s = append(s, 123)
		pool.Put(ptr)
	}
}

func BenchmarkSlicePtrPool_3(b *testing.B) {
	pool := sync.Pool{
		New: func() interface{} {
			s := make([]int, 0, 0)
			return &s
		},
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		ptr := pool.Get().(*[]int)
		s := *ptr
		s = s[0:0]
		s = append(s, 123)
		*ptr = s // Copy the stack header over to the heap
		pool.Put(ptr)
	}
}
