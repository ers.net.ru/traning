package main

import (
	"context"
	"flag"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"time"

	"gopkg.in/yaml.v3"
)

type config struct {
	Name string `yaml:"name"`
}

func main() {
	rand.Seed(time.Now().Unix())

	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt)
	defer stop()

	var configPath string
	flag.StringVar(&configPath, "config", "config.yaml", "Set path to config file.")
	flag.Parse()
	cfg, err := readConfig(configPath)
	if err != nil {
		log.Fatalln("failed read config", err)
	}
	log.Printf("Loaded config: %+v", cfg)

	<-ctx.Done()
}

func readConfig(fileName string) (config, error) {
	var cfg config
	data, err := os.ReadFile(fileName)
	if err != nil {
		return config{}, err
	}
	err = yaml.Unmarshal(data, &cfg)
	if err != nil {
		return config{}, err
	}
	return cfg, nil
}
